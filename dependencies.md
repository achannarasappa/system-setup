## Dependencies
* command line tools
  * general purpose
    * zsh
    * curl
    * httpie
    * tmux
    * jq
    * neovim
    * xclip
    * htop
  * dev
    * docker
    * docker-compose
    * sqlite
    * postgres
    * elixir
    * nodejs
* desktop
  * general purpose
    * virtualbox
    * keepass
    * guake
    * libreoffice
    * chrome
  * dev
    * vscode